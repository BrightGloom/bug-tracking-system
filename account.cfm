
<form id="update-user" name="update-user" method="post">

	<input id="username" type="text" name="username" value="{{ userData.USERNAME }}">
    <label for="username">
        Username:
    </label>

	<input id="first-name" type="text" name="first_name" value="{{ userData.FIRST_NAME }}">
    <label for="first-name">
        First name:
    </label>

	<input id="last-name" type="text" name="last_name" value="{{ userData.LAST_NAME }}">
    <label for="last-name">
        Last name:
    </label>
	<input type="submit" value="Save">
</form>

<script type="text/javascript" src="scripts/account.js"></script>
