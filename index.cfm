<head>
    <meta charset="utf-8">
    <title>
        Bug tracking system
    </title>
    </meta>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"
            grity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.29.2/js/jquery.tablesorter.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-alpha.2/js/materialize.min.js"></script>
    <script  src="scripts/sammy.min.js" type="text/javascript"></script>
    <script  src="scripts/sammy.template.js" type="text/javascript"></script>
    <script  src="scripts/mustache.min.js" type="text/javascript"></script>
    <script  src="scripts/sammy.mustache.js" type="text/javascript"></script>
    <script  src="scripts/toastr.min.js" type="text/javascript"></script>


    <script type="text/javascript" src="scripts/main.js"></script>
    <script  src="scripts/app.js" type="text/javascript"></script>



    <link rel="stylesheet" href="styles/styles.css">
    <link rel="stylesheet" href="styles/toastr.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.29.2/css/theme.materialize.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-alpha.2/css/materialize.min.css">

</head>
<cfinclude template="topMenu.cfm" />
<body>

<div id="app">
    <!-- template will be injected here -->
</div>


</body>


</html>


