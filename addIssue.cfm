
<form name="add-issue" id="add-issue" method="post">
    <textarea id="descr" name="descr" maxlength="100"></textarea>
    <label for="descr">Description:</label>

    <textarea id="short-descr" name="short_descr" maxlength="300"></textarea>
    <label for="short-descr">Short description:</label>

    <select id="status" name="status"></select>
    <label for="status">Status:</label>

    <select id="urgency" name="urgency"></select>
    <label for="urgency">Urgency:</label>

    <select id="criticality" name="criticality"></select>
    <label for="criticality">Criticality:</label>

    <input type="submit" value="Save">
</form>
<script type="text/javascript" src="scripts/addIssue.js"></script>
