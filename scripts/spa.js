function callPage(pageRef) {
    $.get({
        url: pageRef,
        dataType: 'text',
        success: function (response) {
            $('#content').html(response);
            document.location.hash = pageRef;
        },
        error: function (error) {
        }
    })
}

$(document).ready(function(){
	$('#top-menu a').click( function(e) {
		e.preventDefault( );
	    var pageRef = $(this).attr('href');
	  
	    callPage(pageRef);
	})
});