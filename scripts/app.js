(function($) {

    var app = $.sammy('#app', function() {
        // this.use('Template');
        this.use('Mustache', 'cfm');

        this.around(function(callback) {
            var hash = document.location.hash;
            if (!localStorage.getItem('token') && hash != '#/signIn/' && hash != '#/signUp/') {
                this.redirect('#/signIn/');
            } else {
                callback();
            }
        });

        this.get('#/', function(context) {
            context.app.swap('');
            context.render('index.cfm').appendTo(context.$element());
        });

        this.get('#/signIn/', function(context) {
            context.app.swap('');
            context.render('signIn.cfm', {}).appendTo(context.$element());
        });
        this.get('#/signUp/', function(context) {
            context.app.swap('');
            context.render('signUp.cfm', {}).appendTo(context.$element());
        });
        this.get('#/addIssue/', function(context) {
            context.app.swap('');
            context.render('addIssue.cfm', {}).appendTo(context.$element());
        });
        this.get('#/editIssue/:id', function(context) {
            context.app.swap('');
            context.render('editIssue.cfm' + '?id=' + context.params['id'], {}).appendTo(context.$element());
        });
        this.get('#/issues/', function(context) {
            context.app.swap('');
            context.render('issues.cfm', {}).appendTo(context.$element());
        });
        this.get('#/addUser/', function(context) {
            context.app.swap('');
            context.render('addUser.cfm', {}).appendTo(context.$element());
        });
        this.get('#/account/', function(context) {
            context.app.swap('');
            $.ajax({
                type: 'POST',
                url: 'components/authService.cfc',
                data: {
                    method: "getUser"
                },
                headers: {
                    "Authorization": localStorage.getItem('token')
                },
                returnFormat:"json",
                success: function(res) {
                    var result = JSON.parse(res);
                    if(result && result.success) {
                        this.userData = JSON.parse(result.user).DATA;
                    }
                    context.render('account.cfm', {userData: this.userData}).appendTo(context.$element());
                },
                error: function () {
                    document.dispatchEvent(logoutEvent);
                }
            });
        });
        this.get('#/logout', function () {
            localStorage.removeItem('token');
            this.redirect('#/signIn/');
        });

        this.post('#/login', function (context) {
            console.log(context);
        });

        this.before('.*', function() {
            var topMenu = $('#top-menu'),
                hash = document.location.hash;



            if (hash === '#/signIn/' || hash === '#/signUp/') {
                topMenu.hide();
            } else {
                topMenu.show();
                topMenu.find("a").removeClass("current");
                topMenu.find("a[href='"+hash+"']").addClass("current");
            }
        });

    });

    $(function() {
        app.run('#/signIn/');
    });

})(jQuery);