$(document).ready(function(){
    $('#edit-issue').submit(function (e) {
        e.preventDefault();

        var data = {};
        $(this).serializeArray().map(function (item, i) {
            data[item.name] = item.value
        });

        $.ajax({
            type: 'POST',
            url: 'components/issuesService.cfc',
            data: {
                method: "updateIssue",
                argumentCollection: JSON.stringify(data)
            },
            headers: {
                "Authorization": token
            },
            returnFormat:"json",
            success: function(res) {
                var result = JSON.parse(res);
                if (result && result.success) {
                    toastr.success('Issue updated', 'Success!');
                } else {
                    toastr.error(result.message, 'Error!');
                }
            },
            error: function () {
                // document.dispatchEvent(logoutEvent);
            }
        });



    })

});