$(document).ready(function() {

    $('#add-user').on('submit', function (e) {
        e.preventDefault();

        var data = {};
        $(this).serializeArray().map(function (item, i) {
            data[item.name] = item.value
        });

        $.ajax({
            type: 'POST',
            url: 'components/authService.cfc',
            data: {
                method: "addUser",
                argumentCollection: JSON.stringify(data)
            },
            headers: {
                "Authorization": token
            },
            returnFormat:"json",
            success: function(res) {
                var result = JSON.parse(res);
                if(result && result.success) {

                } else {

                }
            },
            error: function () {
                // document.dispatchEvent(logoutEvent);
            }
        });

    })

});