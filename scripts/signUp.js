$(document).ready(function(){
	$('#sign-up').on('submit', function(e) {
	    //Prevent the form from submitting normally
	    e.preventDefault();
	    
	    var data = {};
	    $( this ).serializeArray().map(function(item, i) {
	    	data[item.name] = item.value
	    });
	
	    $.post('components/authService.cfc', { 
	    	method: "signUp",
	    	argumentCollection: JSON.stringify(data),
	    	returnFormat:"json"
	    }, function(res) {
            var result = JSON.parse(res);
            if (result && result.success) {
                toastr.success('User successfully created', 'Success!');
            } else {
                toastr.error(result.message, 'Error!');
            }
	    });
	});
});