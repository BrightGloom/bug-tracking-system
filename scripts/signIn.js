$(document).ready(function() {

    $('#sign-in').on('submit', function (e) {
        e.preventDefault();

        var data = {};
        $(this).serializeArray().map(function (item, i) {
            if (!item.value.trim()) {
                toastr.error('Username and password shouldn\'t be empty');
                return;
            }
            data[item.name] = item.value
        });

        $.post('components/authService.cfc', {
            method: "signIn",
            argumentCollection: JSON.stringify(data),
            returnFormat: "json"
        }, function (res) {
            var result = JSON.parse(res);
            if (result && result.success) {
                localStorage.setItem('token', result.token);
                document.dispatchEvent(signInEvent);
                $(location).attr("href", '#/issues/');
            } else {
                toastr.error(result.message, 'Error!')
            }
        });
    });
});