$(document).ready(function(){
    var variables = {
        status: ['Created', 'Opened', 'Solved', 'Closed'],
        urgency: ['Emergency', 'Urgent', 'Normal', 'Low'],
        criticality: ['Crash', 'Critical', 'Not critical', 'Request for change']
    };

    Object.keys(variables).map(function(objectKey, index) {
        var options = variables[objectKey],
            element = $('#'+objectKey);
        if (!element.innerHTML) {
            options.forEach(function (option) {
                element.append($('<option value=' + option + '>' + option + '</option>'))
            });
        }
    });

    $('#add-issue').submit(function (e) {
        e.preventDefault();

        var data = {};
        $(this).serializeArray().map(function (item, i) {
            data[item.name] = item.value
        });

        $.ajax({
            type: 'POST',
            url: 'components/issuesService.cfc',
            data: {
                method: "addIssue",
                argumentCollection: JSON.stringify(data)
            },
            headers: {
                "Authorization": token
            },
            returnFormat:"json",
            success: function(res) {
                var result = JSON.parse(res);
                if (result && result.success) {
                    toastr.success('Issue created', 'Success!');
                } else {
                    toastr.error(result.message, 'Error!');
                }
            },
            error: function () {
                // document.dispatchEvent(logoutEvent);
            }
        });



    })

});