<form id="add-user" name="add-user" method="post">

    <input id="username" type="text" name="username" value="{{ userData.USERNAME }}">
    <label for="username">
        Username:
    </label>

    <input id="first-name" type="text" name="first_name" value="{{ userData.FIRST_NAME }}">
    <label for="first-name">
        First name:
    </label>

    <input id="last-name" type="text" name="last_name" value="{{ userData.LAST_NAME }}">
    <label for="last-name">
        Last name:
    </label>

    <input id="password" name="password" type="password">
    <label for="password">
        Password:
    </label>

    <input type="submit" value="Save">
</form>

<script type="text/javascript" src="scripts/addUser.js"></script>
