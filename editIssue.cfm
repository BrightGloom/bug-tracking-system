<cfset variables = {
    status: ['Created', 'Opened', 'Solved', 'Closed'],
    urgency: ['Emergency', 'Urgent', 'Normal', 'Low'],
    criticality: ['Crash', 'Critical', 'Not critical', 'Request for change']
}>
<cfset issueData = application.issuesService.getIssue(URL.id)>
<cfset issue = deserializeJSON(issueData.issue).DATA>
<cfset history = deserializeJSON(issueData.history)>
<cfoutput>
    <form name="edit-issue" id="edit-issue" method="post">
            <input name="id" type="hidden" value="#URL.id#">

<textarea id="descr" name="descr" maxlength="100">#issue.DESCR[1]#</textarea>
    <label for="descr">Description:</label>

<textarea id="short-descr" name="short_descr" maxlength="300">#issue.SHORT_DESCR[1]#</textarea>
    <label for="short-descr">Short description:</label>

<select id="status" name="status">
    <cfloop array="#variables.status#" item="item">
        <cfoutput>
                <option value="#item#" <cfif item EQ #issue.STATUS[1]#>selected</cfif> > #item#</option>
        </cfoutput>
    </cfloop>
    </select>
        <label for="status">Status:</label>

    <select id="urgency" name="urgency">
    <cfloop array="#variables.urgency#" item="item">
        <cfoutput>
                <option value="#item#" <cfif item EQ #issue.URGENCY[1]#>selected</cfif> >#item#</option>
        </cfoutput>
    </cfloop>
    </select>
        <label for="urgency">Urgency:</label>

    <select id="criticality" name="criticality">
    <cfloop array="#variables.criticality#" item="item">
        <cfoutput>
                <option value="#item#" <cfif item EQ #issue.CRITICALITY[1]#>selected</cfif> >#item#</option>
        </cfoutput>
    </cfloop>
    </select>
        <label for="criticality">Criticality:</label>

        <textarea id="comment" name="comment" maxlength="300"></textarea>
        <label for="comment">Comment:</label>

        <input type="submit" value="Save">
    </form>

    <div class="history-wrapper">
    <table class="history">
        <thead>
        <th>Action</th>
        <th>User</th>
        <th>Comment</th>
        <th>Date</th>
        </thead>
    <cfloop from="1" to="#history.ROWCOUNT#" index="index">
            <tr>
            <td>
                Status has changed to #history.DATA.ACTION[index]#
            </td>
            <td>
            #history.DATA.USERNAME[index]#
            </td>
            <td>
            #history.DATA.COMMENT[index]#
            </td>
            <td>
            #history.DATA.DATE[index]#
            </td>
            </tr>
    </cfloop>
    </table>
    </div>

</cfoutput>

<script type="text/javascript" src="scripts/editIssue.js"></script>
