<div class="auth-wrapper">
    <form name="sign-up" id="sign-up" method="post">
        <label>Username:</label>
        <input type="text" name="username" required>
        <label>Password:</label>
        <input type="password" name="password" required>
        <br>
        <input type="submit" value="Sign up">
    </form>
    <a id="sign-in-btn" href="#/signIn/">
        Sign in
    </a>
</div>

<script type="text/javascript" src="scripts/signUp.js"></script>
