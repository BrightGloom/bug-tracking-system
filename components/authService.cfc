<cfcomponent output="false">
	<!---validateUser() method --->
	<cffunction name="validateUser" access="public" output="false" returntype="array">
		<cfargument name="username" type="string" required="true" />
		<cfargument name="password" type="string" required="true" />
		
		<cfset errors = ArrayNew(1)>
		
		<cfif arguments.username EQ ''>
			<cfset arrayAppend(errors, 'Please provide a username') />
		</cfif>
		
		<cfif arguments.password EQ ''>
			<cfset arrayAppend(errors, 'Please provide a password') />
		</cfif>
		
		<cfreturn errors />
	</cffunction>

	<!---signIn() method --->
	<cffunction name="signIn" access="remote" output="false" returntype="struct" returnformat="json" produces="application/json">
		<cfargument name="username" type="string" required="true" />
		<cfargument name="password" type="string" required="true" />
		
		<cfset var response = {} />

		<cfquery name="rsSignIn">
			SELECT id, username, password, salt
			FROM users
			WHERE username = <cfqueryparam value="#arguments.username#" cfsqltype="CF_SQL_VARCHAR" />
		</cfquery>
		<cfif rsSignIn.recordCount EQ 1>
			<cfset decryptedPassword=decrypt(#rsSignIn.password#, #rsSignIn.salt#, "AES", "Base64") />
			<cfif decryptedPassword EQ arguments.password>
				<cfset expDate = dateAdd('d', 30, now()) />
				<cfset jwt = new jwt(application.jwtkey) />
				<cfset payload = {"ts" = now(), "id" = #rsSignIn.id#, "exp" = #expDate#} />
				<cfset token = jwt.encode(payload) />
				<cfset response['success'] = true />
				<cfset response['message'] = "Successfully signed in" />
				<cfset response['token'] = token />
            <cfelse>
                <cfset response['success'] = false />
                <cfset response['message'] = "Username or password is incorrect" />
			</cfif>
		<cfelse>
			<cfset response['success'] = false />
			<cfset response['message'] = "Username or password is incorrect" />
		</cfif>
		<cfreturn response />
	</cffunction>

	<!---checkToken() method --->
	<cffunction name="checkToken" access="remote" output="false" returntype="struct" returnformat="json">
		<cfset var response = {}>
		<cfset requestData = getHTTPRequestData()>
		<cfif structKeyExists(requestData.headers, "authorization") >
			<cfset token = requestData.headers.authorization>
			<cftry>
				<cfset jwt = new jwt(application.jwtkey)>
				<cfset result = jwt.decode(token)>

				<cfset response['success'] = true>

				<cfcatch type="any">
					<cfset response['success'] = false>
					<cfset response['message'] = cfcatch.message>
					<cfreturn response>
				</cfcatch>
			</cftry>
		<cfelse>
			<cfset response['success'] = false />
			<cfset response['message'] = "Token is invalid or expired" />
		</cfif>
		<cfreturn response>

	</cffunction>

	<!---signUp() method --->
	<cffunction name="signUp" access="remote" output="false" returntype="struct" returnformat="json">
		<cfargument name="username" type="string" required="true" />
		<cfargument name="password" type="string" required="true" />
		
		<cfset var response = {} />
		<cfset password = #arguments.password# />
		<cfset key = GenerateSecretKey("AES") />
		<cfset encryptedPassword = encrypt(#password#, #key#, "AES", "Base64")>

        <cfquery name="checkUsernameAvailability">
            SELECT id
            FROM users
            WHERE username = <cfqueryparam value="#arguments.username#" cfsqltype="CF_SQL_LONGVARCHAR">
        </cfquery>

        <cfif checkUsernameAvailability.recordCount EQ 1>
            <cfset response['success'] = false>
            <cfset response['message'] = 'Username already used by another user'>
            <cfreturn response>
        </cfif>

		<cftry>
            <cfquery name="rsSignUp">
                INSERT INTO users
                (username, password, salt)
                VALUES
                (
                    <cfqueryparam value="#arguments.username#" cfsqltype="CF_SQL_VARCHAR" />,
                    <cfqueryparam value="#encryptedPassword#" cfsqltype="CF_SQL_VARCHAR" />,
                    <cfqueryparam value="#key#" cfsqltype="CF_SQL_VARCHAR" />
                )
            </cfquery>
            <cfcatch>
                <cfset response['success'] = false>
                <cfset response['message'] = cfcatch.message>
                <cfreturn response>
            </cfcatch>
		</cftry>
        <cfset response['success'] = true />
        <cfset response['message'] = "User created" />
		<cfreturn response>
	</cffunction>

    <!---addUser() method --->
    <cffunction name="addUser" access="remote" output="false" returntype="struct" returnformat="json">
        <cfargument name="username" type="string" required="true" />
        <cfargument name="first_name" type="string" required="true" />
        <cfargument name="last_name" type="string" required="true" />
        <cfargument name="password" type="string" required="true" />

        <cfset var response = {} />
        <cfset password = #arguments.password# />
        <cfset key = GenerateSecretKey("AES") />
        <cfset encryptedPassword = encrypt(#password#, #key#, "AES", "Base64")>


        <cfquery name="rsAddUser">
			INSERT INTO users
			(username, first_name, last_name, password, salt)
			VALUES
			(
				<cfqueryparam value="#arguments.username#" cfsqltype="CF_SQL_VARCHAR" />,
				<cfqueryparam value="#arguments.first_name#" cfsqltype="CF_SQL_VARCHAR" />,
				<cfqueryparam value="#arguments.last_name#" cfsqltype="CF_SQL_VARCHAR" />,
				<cfqueryparam value="#encryptedPassword#" cfsqltype="CF_SQL_VARCHAR" />,
				<cfqueryparam value="#key#" cfsqltype="CF_SQL_VARCHAR" />
			)
		</cfquery>

        <cfset response['success'] = true />
        <cfset response['message'] = "User created" />

        <cfreturn response>
    </cffunction>

    <!---updateUser() method --->
    <cffunction name="updateUser" access="remote" output="false" returntype="struct" returnformat="json">
        <cfargument name="username" type="string" required="false" />
        <cfargument name="first_name" type="string" required="false" />
        <cfargument name="last_name" type="string" required="false" />

        <cfset var response = {} />

        <cfset requestData = getHTTPRequestData()>
        <cfif structKeyExists(requestData.headers, "authorization") >
            <cfset token = requestData.headers.authorization>
            <cfset decodeToken = getIdFromToken(token)>
            <cfif decodeToken.success>
                <cfset id = decodeToken.id>
                <cfquery name="rsUserUpdate">
                    UPDATE users
                    SET username = <cfqueryparam value="#arguments.username#" cfsqltype="CF_SQL_VARCHAR" />,
                        first_name = <cfqueryparam value="#arguments.first_name#" cfsqltype="CF_SQL_VARCHAR" />,
                        last_name = <cfqueryparam value="#arguments.last_name#" cfsqltype="CF_SQL_VARCHAR" />
                    WHERE id = <cfqueryparam value="#id#" cfsqltype="CF_SQL_NUMERIC" />
                </cfquery>
                <cfset response['success'] = true />
                <cfset response['message'] = "User updated" />
            </cfif>
        </cfif>

        <cfreturn response>
    </cffunction>

    <!---getIdFromToken() method --->
    <cffunction name="getIdFromToken" access="private" output="false" returntype="struct">
        <cfargument name="token" type="string" required="true">
        <cfset response = {}>
        <cftry>
            <cfset jwt = new jwt(application.jwtkey)>
            <cfset result = jwt.decode(token)>

            <cfset response['success'] = true>
            <cfset response['id'] = result.id>

            <cfcatch type="any">
                <cfset response['success'] = false>
                <cfset response['message'] = cfcatch.message>
                <cfreturn response>
            </cfcatch>
        </cftry>

        <cfreturn response>
    </cffunction>

	<!---getUser() method --->
	<cffunction name="getUser" access="remote" output="false" returntype="any" returnformat="json">
		<cfset var response = {}>
		<cfset requestData = getHTTPRequestData()>
		<cfif structKeyExists(requestData.headers, "authorization") >
			<cfset token = requestData.headers.authorization>
            <cfset decodeToken = getIdFromToken(token)>
            <cfif decodeToken.success>
                <cfset id = decodeToken.id>
                <cfquery name="rsGetUser">
                    SELECT username, first_name, last_name
                    FROM users
                    WHERE id = <cfqueryparam value="#result.id#" cfsqltype="cf_sql_numeric">
                </cfquery>
                <cfset response['success'] = true>
                <cfset response['user'] = SerializeJSON(rsGetUser, true)>
            </cfif>
		<cfelse>
			<cfset response['success'] = false>
		</cfif>

		<cfreturn response>
	</cffunction>
	
	
</cfcomponent>