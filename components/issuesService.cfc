<cfcomponent output="false">
<!---getIssues() method --->
    <cffunction name="getIssues" access="public" output="false" returntype="query">
        <cfquery name="rsIssuesList">
			SELECT issues.*,
			       users.username
			FROM issues
			LEFT JOIN users
			ON issues.user = users.id
			ORDER BY issues.date DESC
        </cfquery>
        <cfreturn rsIssuesList>
    </cffunction>

<!---getIssue() method --->
    <cffunction name="getIssue" access="public" output="false" returntype="struct" returnformat="json">
        <cfargument name="id" type="numeric">
        <cfset response = {}>
        <cfquery name="rsIssue">
			SELECT *
			FROM issues
			WHERE issues.id = <cfqueryparam value=#arguments.id# cfsqltype="cf_sql_numeric">
        </cfquery>
        <cfquery name="rsHistory">
			SELECT history.*, users.username
			FROM history
			LEFT JOIN users
			ON history.user = users.id
			WHERE issue = <cfqueryparam value=#arguments.id# cfsqltype="cf_sql_numeric">
            ORDER BY date DESC
        </cfquery>
        <cfset response['issue'] = SerializeJSON(rsIssue, true)>
        <cfset response['history'] = SerializeJSON(rsHistory, true)>
        <cfreturn response>
    </cffunction>

<!---addIssue() method --->
    <cffunction name="addIssue" access="remote" output="false" returntype="struct" returnformat="json">
        <cfargument name="short_descr" type="string">
        <cfargument name="descr" type="string">
        <cfargument name="status" type="string">
        <cfargument name="urgency" type="string">
        <cfargument name="criticality" type="string">

        <cfset requestData = getHTTPRequestData()>
        <cfif structKeyExists(requestData.headers, "authorization") >
            <cfset token = requestData.headers.authorization>
            <cftry>
                <cfset jwt = new jwt(application.jwtkey)>
                <cfset tokenData = jwt.decode(token)>
                <cfcatch type="any">
                    <cfset response['success'] = false>
                    <cfset response['message'] = cfcatch.message>
                    <cfreturn response>
                </cfcatch>
            </cftry>
        </cfif>

        <cfset response = {}>
        <cfset creationDate = DateTimeFormat(now(), "yyyy-mm-dd HH:nn:ss")>
        <cfset user = tokenData.id>

        <cfquery name="rsAddIssue" result="result">
            INSERT INTO issues
            (date, short_descr, descr, user, status, urgency, criticality)
            VALUES
            (
            <cfqueryparam value="#creationDate#" cfsqltype="datetime">,
            <cfqueryparam value="#arguments.short_descr#" cfsqltype="string">,
            <cfqueryparam value="#arguments.descr#" cfsqltype="string">,
            <cfqueryparam value="#user#" cfsqltype="numeric">,
            <cfqueryparam value="#arguments.status#" cfsqltype="string">,
            <cfqueryparam value="#arguments.urgency#" cfsqltype="string">,
            <cfqueryparam value="#arguments.criticality#" cfsqltype="string">
            )
        </cfquery>
        <cfscript>
            pk = result.GENERATEDKEY;
            createHistory(pk, creationDate, user, status, '');
        </cfscript>

        <cfset response['success'] = true>

        <cfreturn response>
    </cffunction>

<!---updateIssue() method --->
    <cffunction name="updateIssue" access="remote" output="false" returntype="any" returnformat="json">
        <cfargument name="id" type="numeric">
        <cfargument name="short_descr" type="string">
        <cfargument name="descr" type="string">
        <cfargument name="status" type="string">
        <cfargument name="urgency" type="string">
        <cfargument name="criticality" type="string">
        <cfargument name="comment" type="string">

        <cfset requestData = getHTTPRequestData()>
        <cfif structKeyExists(requestData.headers, "authorization") >
            <cfset token = requestData.headers.authorization>
            <cftry>
                <cfset jwt = new jwt(application.jwtkey)>
                <cfset tokenData = jwt.decode(token)>
                <cfcatch type="any">
                    <cfset response['success'] = false>
                    <cfset response['message'] = cfcatch.message>
                    <cfreturn response>
                </cfcatch>
            </cftry>
        </cfif>

        <cfset response = {}>
        <cfset creationDate = DateTimeFormat(now(), "yyyy-mm-dd HH:nn:ss")>
        <cfset user = tokenData.id>

        <cfquery name="rsAddIssue" result="result">
            UPDATE issues
            SET
                date = <cfqueryparam value="#creationDate#" cfsqltype="datetime">,
                short_descr = <cfqueryparam value="#arguments.short_descr#" cfsqltype="cf_sql_varchar">,
                descr = <cfqueryparam value="#arguments.descr#" cfsqltype="cf_sql_varchar">,
                user = <cfqueryparam value="#user#" cfsqltype="cf_sql_numeric">,
                status = <cfqueryparam value="#arguments.status#" cfsqltype="cf_sql_varchar">,
                urgency = <cfqueryparam value="#arguments.urgency#" cfsqltype="cf_sql_varchar">,
                criticality = <cfqueryparam value="#arguments.criticality#" cfsqltype="cf_sql_varchar">
            WHERE id = #arguments.id#
        </cfquery>

        <cfscript>
            createHistory(arguments.id, creationDate, user, arguments.status, arguments.comment);
        </cfscript>

    </cffunction>

<!---createHistory() method --->
    <cffunction name="createHistory" access="private" output="false" returntype="any" returnformat="json">
        <cfargument name="id" type="numeric">
        <cfargument name="date" type="date">
        <cfargument name="user" type="numeric">
        <cfargument name="action" type="string">
        <cfargument name="comment" type="string">


        <cfquery name="rsAssHistory">
            INSERT INTO history
            (issue, date, user, action, comment)
            VALUES
            (
                <cfqueryparam value="#arguments.id#" cfsqltype="cf_sql_numeric">,
                <cfqueryparam value="#arguments.date#" cfsqltype="datetime">,
                <cfqueryparam value="#arguments.user#" cfsqltype="cf_sql_numeric">,
                <cfqueryparam value="#arguments.action#" cfsqltype="cf_sql_varchar">,
                <cfqueryparam value="#arguments.comment#" cfsqltype="cf_sql_varchar">
            )
        </cfquery>
    </cffunction>

</cfcomponent>