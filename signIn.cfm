<div class="auth-wrapper">
    <form name="sign-in" id="sign-in" method="post">
        <label>Username:</label>
        <input type="text" name="username" required>
        <label>Password:</label>
        <input type="password" name="password" required>
        <br>
        <input type="submit" value="Sign in">
    </form>
    <a id="sign-up-btn" href="#/signUp/">
        Sign up
    </a>
</div>


<script type="text/javascript" src="scripts/signIn.js"></script>