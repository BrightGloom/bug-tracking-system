<cfcomponent output="false">
	<cfset this.name = 'Test Task' />
	<cfset this.applicationTimeout = createtimespan(0,2,0,0) />
	<cfset this.datasource = 'testtask' />
	<cfset this.sessionManagement = true />
	<cfset this.sessionTimeout = createTimespan(0,0,30,0) />
	
	<!---OnApplicationStart() method--->
	<cffunction name="onApplicationStart" returntype="boolean" >
		<cfset application.authService = createObject("component",'TestTask.components.authService') />
		<cfset application.issuesService = createObject("component",'TestTask.components.issuesService') />
		<cfset application.jwtkey = "sj$fdg!34$" />
		<cfreturn true />
	</cffunction>
	<!---onRequestStart() method--->
	<cffunction name="onRequestStart" returntype="boolean" >
		<cfargument name="targetPage" type="string" required="true" />
		<!---handle some special URL parameters--->
		<cfif isDefined('url.restartApp')>
			<cfset this.onApplicationStart() />
		</cfif>
		<!---Implement ressource Access control for the 'admin' folder--->
		<cfif listFind(arguments.targetPage,'admin', '/') AND (NOT isUserLoggedIn() OR NOT isUserInRole('Administrator'))>
			<cflocation url="/TestTask/index.cfm?noaccess" />
		</cfif>
		<cfreturn true />
	</cffunction>
</cfcomponent>