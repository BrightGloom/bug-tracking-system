<div>
<cfset rsAllIssues = application.issuesService.getIssues() />

<table class="issues">
    <thead>
    <tr class="table-header">
        <th>
            Short description
        </th>
        <th>
            User
        </th>
        <th>
            Modified
        </th>
        <th>
            Status
        </th>
        <th>
            Urgency
        </th>
        <th>
            Criticality
        </th>
        <th>

        </th>
    </tr>
    </thead>


<cfoutput query="rsAllIssues">

        <tr>
        <td>
        #rsAllIssues.descr#
        </td>
        <td>
        #rsAllIssues.username#
        </td>
        <td>
        #rsAllIssues.date#
        </td>
        <td>
        #rsAllIssues.status#
        </td>
        <td>
        #rsAllIssues.urgency#
        </td>
        <td>
        #rsAllIssues.criticality#
        </td>
        <td>
                <a href="##/editIssue/#rsAllIssues.id#">
        Edit
    </a>
    </td>
    </tr>
</cfoutput>

</table>
</div>

<script type="text/javascript" src="scripts/issues.js"></script>